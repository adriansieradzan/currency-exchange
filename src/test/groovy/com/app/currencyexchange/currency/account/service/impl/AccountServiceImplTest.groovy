package com.app.currencyexchange.currency.account.service.impl

import com.app.currencyexchange.currency.account.model.AccountDto
import com.app.currencyexchange.currency.account.model.AccountRegistration
import com.app.currencyexchange.currency.account.repository.AccountRepository
import com.app.currencyexchange.currency.account.service.AccountService
import com.app.currencyexchange.currency.account.service.mapper.AccountDtoToEntityMapper
import com.app.currencyexchange.entity.account.Account
import com.app.currencyexchange.exception.BadRequestException
import spock.lang.Specification

class AccountServiceImplTest extends Specification {

    private static final String FIRST_NAME = "firstName"
    private static final String LAST_NAME = "lastName"
    private static final String PESEL = "11111111116"
    private static final BigDecimal MONEY_AMOUNT = new BigDecimal(50)
    private static final int ACCOUNT_ID = 1

    private AccountDtoToEntityMapper accountDtoToEntityMapper = Mock()
    private AccountRepository accountRepository = Mock()
    private AccountService accountService

    void setup() {
        accountService = new AccountServiceImpl(accountDtoToEntityMapper, accountRepository)
    }

    def "should create account"() {
        given:
        def accountRegistration = accountRegistration()
        when:
        accountService.createAccount(accountRegistration)

        then:
        1 * accountRepository.existsByPesel(PESEL) >> Boolean.FALSE
        1 * accountDtoToEntityMapper.accountRegistrationToEntity(_) >> new Account()
        1 * accountRepository.save(_)
        0 * _
    }

    def "should throw exception when user is exist"() {
        given:
        def accountRegistration = accountRegistration()
        when:
        accountService.createAccount(accountRegistration)

        then:
        1 * accountRepository.existsByPesel(PESEL) >> Boolean.TRUE
        thrown(BadRequestException)
        0 * _
    }


    def "should get account"() {
        given:
        def account = account()
        when:
        accountService.getAccount(PESEL)

        then:
        1 * accountRepository.findByPesel(PESEL) >> Optional.ofNullable(account)
        1 * accountDtoToEntityMapper.accountToDto(account) >> new AccountDto()
        0 * _
    }

    def "should throw exception when user doesn't exist"() {
        given:

        when:
        accountService.getAccount(PESEL)

        then:
        1 * accountRepository.findByPesel(PESEL) >> Optional.ofNullable(null)
        thrown(BadRequestException)
        0 * _
    }

    private static AccountRegistration accountRegistration() {
        return AccountRegistration.builder()
            .firstName(FIRST_NAME)
            .lastName(LAST_NAME)
            .pesel(PESEL)
            .moneyAmount(MONEY_AMOUNT)
            .build()
    }

    private static Account account() {
        Account account = new Account()
        account.setId(ACCOUNT_ID)
        account.setPesel(PESEL)
        account.setFirstName(FIRST_NAME)
        return account;
    }

}
