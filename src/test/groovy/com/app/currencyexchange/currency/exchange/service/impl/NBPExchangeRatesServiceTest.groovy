package com.app.currencyexchange.currency.exchange.service.impl


import com.app.currencyexchange.currency.exchange.model.ExchangePair
import com.app.currencyexchange.currency.exchange.model.ExchangeRate
import com.app.currencyexchange.currency.exchange.model.NBPTableCCurrency
import com.app.currencyexchange.currency.exchange.model.Rates
import com.app.currencyexchange.currency.exchange.service.ExchangeRatesService
import com.app.currencyexchange.entity.currencyaccount.Currency
import com.app.currencyexchange.exception.CurrencyNotFoundException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class NBPExchangeRatesServiceTest extends Specification {

    private static final String NAP_API_EXCHANGE_RATES = "http://api.nbp.pl/api/exchangerates/rates/C/%s/";
    public static final BigDecimal BID_VALUE = 4.5
    public static final BigDecimal ASK_VALUE = 4.2

    private RestTemplate restTemplate = Mock()
    private ExchangeRatesService exchangeRatesService

    void setup() {
        exchangeRatesService = new NBPExchangeRatesService(restTemplate)
    }

    def "should get actual usd to pln exchange rate"() {
        given:
        def exchangePair = ExchangePair.USD_PLN
        def nbpTableCCurrency = nbpTableCCurrency()
        when:
        ExchangeRate exchangeRate = exchangeRatesService.getExchangeForCurrency(exchangePair)

        then:
        1 * restTemplate.getForObject(String.format(NAP_API_EXCHANGE_RATES, Currency.USD), NBPTableCCurrency.class) >> nbpTableCCurrency
        exchangeRate.getBuyValue() == BID_VALUE
        exchangeRate.getSellValue() == ASK_VALUE
        0 * _
    }

    def "should get actual pln to usd exchange rate"() {
        given:
        def exchangePair = ExchangePair.PLN_USD
        def nbpTableCCurrency = nbpTableCCurrency()
        when:
        ExchangeRate exchangeRate = exchangeRatesService.getExchangeForCurrency(exchangePair)

        then:
        1 * restTemplate.getForObject(String.format(NAP_API_EXCHANGE_RATES, Currency.USD), NBPTableCCurrency.class) >> nbpTableCCurrency
        exchangeRate.getBuyValue() == BigDecimal.ONE.divide(ASK_VALUE, 10, BigDecimal.ROUND_HALF_UP)
        exchangeRate.getSellValue() == BigDecimal.ONE.divide(BID_VALUE, 10, BigDecimal.ROUND_HALF_UP)
        0 * _
    }


    def "should throw exception where is no currency"() {
        given:
        def exchangePair = ExchangePair.USD_PLN
        when:
        ExchangeRate exchangeRate = exchangeRatesService.getExchangeForCurrency(exchangePair)

        then:
        1 * restTemplate.getForObject(String.format(NAP_API_EXCHANGE_RATES, Currency.USD), NBPTableCCurrency.class) >> null
        thrown(CurrencyNotFoundException)
        0 * _
    }

    NBPTableCCurrency nbpTableCCurrency() {
        NBPTableCCurrency nbpTableCCurrency = new NBPTableCCurrency()
        Rates rates = new Rates()
        rates.setAsk(ASK_VALUE)
        rates.setBid(BID_VALUE)
        nbpTableCCurrency.setRates([rates])
        return nbpTableCCurrency
    }
}
