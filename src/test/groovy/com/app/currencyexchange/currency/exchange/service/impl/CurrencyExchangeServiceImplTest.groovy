package com.app.currencyexchange.currency.exchange.service.impl


import com.app.currencyexchange.currency.account.model.Action
import com.app.currencyexchange.currency.account.repository.AccountRepository
import com.app.currencyexchange.currency.account.service.mapper.AccountDtoToEntityMapper
import com.app.currencyexchange.currency.exchange.model.ExchangePair
import com.app.currencyexchange.currency.exchange.model.ExchangeRate
import com.app.currencyexchange.currency.exchange.model.ExchangeValue
import com.app.currencyexchange.currency.exchange.service.CurrencyExchangeService
import com.app.currencyexchange.currency.exchange.service.ExchangeRatesService
import com.app.currencyexchange.entity.account.Account
import com.app.currencyexchange.entity.currencyaccount.Currency
import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount
import com.app.currencyexchange.exception.BadRequestException
import spock.lang.Specification

class CurrencyExchangeServiceImplTest extends Specification {

    private static final String FIRST_NAME = "firstName"
    private static final String PESEL = "11111111116"
    private static final int ACCOUNT_ID = 1
    public static final BigDecimal AMOUNT_TO_EXCHANGE = 10
    public static final BigDecimal TO_BIG_AMOUNT_TO_EXCHANGE = 51
    public static final BigDecimal AMOUNT_ON_PLN_SUB_ACCOUNT = 50
    public static final BigDecimal AMOUNT_ON_USD_SUB_ACCOUNT = 10
    public static final BigDecimal BUY_VALUE_USD_1 = 4.5
    public static final BigDecimal SELL_VALUE_USD_1 = 4.2

    private ExchangeRatesService exchangeRatesService = Mock()
    private AccountRepository accountRepository = Mock()
    private AccountDtoToEntityMapper accountDtoToEntityMapper = Mock()
    private CurrencyExchangeService currencyExchangeService

    void setup() {
        currencyExchangeService = new CurrencyExchangeServiceImpl(exchangeRatesService, accountRepository, accountDtoToEntityMapper)
    }

    def "should buy USD from PLN"() {

        given:
        def exchangeValue = exchangeValueBuy()
        def account = account()
        when:
        currencyExchangeService.exchangeValue(exchangeValue)
        then:
        1 * accountRepository.findByPesel(PESEL) >> Optional.of(account)
        1 * exchangeRatesService.getExchangeForCurrency(exchangeValue.getExchangePair()) >> exchangeRate(BUY_VALUE_USD_1, SELL_VALUE_USD_1)
        1 * accountRepository.save(account) >> account
        1 * accountDtoToEntityMapper.accountToDto(account)
        0 * _
    }

    def "should sell PLN to USD"() {

        given:
        def exchangeValue = exchangeValueSell()
        def account = account()
        when:
        currencyExchangeService.exchangeValue(exchangeValue)
        then:
        1 * accountRepository.findByPesel(PESEL) >> Optional.of(account)
        1 * exchangeRatesService.getExchangeForCurrency(exchangeValue.getExchangePair()) >> exchangeRate(BUY_VALUE_USD_1, SELL_VALUE_USD_1)
        1 * accountRepository.save(account) >> account
        1 * accountDtoToEntityMapper.accountToDto(account)
        0 * _
    }

    def "should throw Exception where is not enough money to exchange"() {

        given:
        def exchangeValue = exchangeValueSellWhereIsNotEnoughMoney()
        def account = account()
        when:
        currencyExchangeService.exchangeValue(exchangeValue)
        then:
        1 * accountRepository.findByPesel(PESEL) >> Optional.of(account)
        1 * exchangeRatesService.getExchangeForCurrency(exchangeValue.getExchangePair()) >> exchangeRate(BUY_VALUE_USD_1, SELL_VALUE_USD_1)
        thrown(BadRequestException)
        0 * _
    }

    ExchangeValue exchangeValueBuy() {
        return ExchangeValue.builder()
            .exchangePair(ExchangePair.USD_PLN)
            .pesel(PESEL)
            .amount(new BigDecimal(AMOUNT_TO_EXCHANGE))
            .action(Action.BUY)
            .build()
    }

    ExchangeValue exchangeValueSell() {
        return ExchangeValue.builder()
            .exchangePair(ExchangePair.USD_PLN)
            .pesel(PESEL)
            .amount(new BigDecimal(AMOUNT_TO_EXCHANGE))
            .action(Action.SELL)
            .build()
    }

    ExchangeValue exchangeValueSellWhereIsNotEnoughMoney() {
        return ExchangeValue.builder()
            .exchangePair(ExchangePair.USD_PLN)
            .pesel(PESEL)
            .amount(new BigDecimal(TO_BIG_AMOUNT_TO_EXCHANGE))
            .action(Action.SELL)
            .build()
    }

    private static Account account() {
        Account account = new Account()
        account.setId(ACCOUNT_ID)
        account.setPesel(PESEL)
        account.setFirstName(FIRST_NAME)
        account.setCurrencyAccounts([currencyAccount(AMOUNT_ON_PLN_SUB_ACCOUNT, Currency.PLN), currencyAccount(AMOUNT_ON_USD_SUB_ACCOUNT, Currency.USD)] as Set)
        return account;
    }

    private static CurrencyAccount currencyAccount(BigDecimal amount, Currency currency) {
        CurrencyAccount currencyAccount = new CurrencyAccount()
        currencyAccount.setAmount(amount)
        currencyAccount.setCurrency(currency)
        return currencyAccount;
    }

    private ExchangeRate exchangeRate(BigDecimal buyValue, BigDecimal sellValue) {
        return ExchangeRate.builder()
            .buyValue(buyValue)
            .sellValue(sellValue)
            .build()
    }
}
