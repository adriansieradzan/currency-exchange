package com.app.currencyexchange.currency.account.model;

import com.app.currencyexchange.constraint.AdultByPesel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {

    @NotBlank
    @Size(min = 2, max = 100)
    private String firstName;
    @NotBlank
    @Size(min = 2, max = 100)
    private String lastName;
    @AdultByPesel
    @PESEL
    @NotBlank
    private String pesel;
    private Set<CurrencyAccountDto> currencyAccounts;
}
