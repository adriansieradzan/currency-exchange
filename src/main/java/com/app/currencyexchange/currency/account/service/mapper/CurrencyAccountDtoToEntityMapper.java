package com.app.currencyexchange.currency.account.service.mapper;

import com.app.currencyexchange.currency.account.model.CurrencyAccountDto;
import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CurrencyAccountDtoToEntityMapper {

    CurrencyAccountDto currencyAccountToDto(CurrencyAccount currencyAccount);

    @Mapping(target = "id", ignore = true)
    CurrencyAccount currencyAccountToEntity(CurrencyAccountDto currencyAccount);

}
