package com.app.currencyexchange.currency.account.service.mapper;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.account.model.AccountRegistration;
import com.app.currencyexchange.entity.account.Account;
import com.app.currencyexchange.entity.currencyaccount.Currency;
import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = CurrencyAccountDtoToEntityMapper.class)
public interface AccountDtoToEntityMapper {

    AccountDto accountToDto(Account account);

    @Mapping(target = "id", ignore = true)
    Account accountToEntity(AccountDto account);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "currencyAccounts", ignore = true)
    Account accountRegistrationToEntity(AccountRegistration accountRegistration);

    @AfterMapping
    default void setCurrencyAccount(AccountRegistration accountRegistration, @MappingTarget Account account) {
        CurrencyAccount currencyAccount = CurrencyAccount.builder()
            .amount(accountRegistration.getMoneyAmount())
            .currency(Currency.PLN)
            .build();
        account.addCurrencyAccount(currencyAccount);
    }

}
