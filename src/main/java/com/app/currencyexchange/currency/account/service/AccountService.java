package com.app.currencyexchange.currency.account.service;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.account.model.AccountRegistration;

public interface AccountService {

    void createAccount(AccountRegistration accountDto);

    AccountDto getAccount(String pesel);
}
