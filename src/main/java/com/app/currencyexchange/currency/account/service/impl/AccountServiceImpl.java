package com.app.currencyexchange.currency.account.service.impl;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.account.model.AccountRegistration;
import com.app.currencyexchange.currency.account.repository.AccountRepository;
import com.app.currencyexchange.currency.account.service.AccountService;
import com.app.currencyexchange.currency.account.service.mapper.AccountDtoToEntityMapper;
import com.app.currencyexchange.entity.account.Account;
import com.app.currencyexchange.entity.currencyaccount.Currency;
import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount;
import com.app.currencyexchange.exception.BadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountDtoToEntityMapper accountDtoToEntityMapper;
    private final AccountRepository accountRepository;


    @Override
    public void createAccount(AccountRegistration accountRegistration) {
        validateThereIsARegisteredUser(accountRegistration.getPesel());
        saveAccount(accountRegistration);
    }

    @Override
    public AccountDto getAccount(String pesel) {
        return accountDtoToEntityMapper.accountToDto(accountRepository.findByPesel(pesel).orElseThrow(() ->
            new BadRequestException("Account doesn't exist.")
        ));
    }

    private void validateThereIsARegisteredUser(String pesel) {
        if (accountRepository.existsByPesel(pesel)) {
            throw new BadRequestException("User is already registered.");
        }
    }

    private void saveAccount(AccountRegistration accountRegistration) {
        Account account = accountDtoToEntityMapper.accountRegistrationToEntity(accountRegistration);
        CurrencyAccount currencyAccount = CurrencyAccount.builder()
            .currency(Currency.USD)
            .amount(BigDecimal.ZERO)
            .build();
        account.addCurrencyAccount(currencyAccount);
        accountRepository.save(account);
    }
}
