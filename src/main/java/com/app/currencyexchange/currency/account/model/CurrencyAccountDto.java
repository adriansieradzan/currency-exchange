package com.app.currencyexchange.currency.account.model;

import com.app.currencyexchange.entity.currencyaccount.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyAccountDto {

    private BigDecimal amount;
    private Currency currency;
}
