package com.app.currencyexchange.currency.account.controller;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.account.model.AccountRegistration;
import com.app.currencyexchange.currency.account.service.AccountService;
import lombok.AllArgsConstructor;
import org.hibernate.validator.constraints.pl.PESEL;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(AccountController.BASE_URL)
public class AccountController {

    public static final String BASE_URL = "/api/account";

    private final AccountService accountService;

    @PostMapping("/")
    public void createAccount(@Validated @RequestBody AccountRegistration account) {
        accountService.createAccount(account);
    }

    @GetMapping("/{pesel}")
    public AccountDto getAccount(@PESEL @PathVariable String pesel) {
        return accountService.getAccount(pesel);
    }


}
