package com.app.currencyexchange.currency.account.repository;

import com.app.currencyexchange.entity.account.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    Boolean existsByPesel(String pesel);
    Optional<Account> findByPesel(String pesel);
}
