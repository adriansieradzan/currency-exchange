package com.app.currencyexchange.currency.account.model;

import com.app.currencyexchange.constraint.AdultByPesel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountRegistration {

    @NotBlank
    @Size(min = 2, max = 100)
    private String firstName;
    @NotBlank
    @Size(min = 2, max = 100)
    private String lastName;
    @PESEL
    @AdultByPesel
    @NotBlank
    private String pesel;
    @Min(1)
    private BigDecimal moneyAmount;
}
