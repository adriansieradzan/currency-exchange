package com.app.currencyexchange.currency.account.model;

public enum Action {
    BUY, SELL
}
