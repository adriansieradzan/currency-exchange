package com.app.currencyexchange.currency.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NBPTableCCurrency {

    private String table;
    private String currency;
    private String code;
    List<Rates> rates = new ArrayList<>();

}
