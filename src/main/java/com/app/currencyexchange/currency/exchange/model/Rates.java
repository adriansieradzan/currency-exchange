package com.app.currencyexchange.currency.exchange.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rates {

    private String no;
    private String effectiveDate;
    private BigDecimal bid;
    private BigDecimal ask;

}
