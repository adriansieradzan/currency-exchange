package com.app.currencyexchange.currency.exchange.service.impl;

import com.app.currencyexchange.currency.exchange.model.ExchangePair;
import com.app.currencyexchange.currency.exchange.model.ExchangeRate;
import com.app.currencyexchange.currency.exchange.model.NBPTableCCurrency;
import com.app.currencyexchange.currency.exchange.service.ExchangeRatesService;
import com.app.currencyexchange.entity.currencyaccount.Currency;
import com.app.currencyexchange.exception.CurrencyNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@AllArgsConstructor
public class NBPExchangeRatesService implements ExchangeRatesService {

    private static final String NAP_API_EXCHANGE_RATES = "http://api.nbp.pl/api/exchangerates/rates/C/%s/";

    private final RestTemplate restTemplate;

    @Override
    public ExchangeRate getExchangeForCurrency(ExchangePair exchangePair) {

        if (exchangePair.getReversed()) {
            NBPTableCCurrency result = getExchangeRates(restTemplate, exchangePair.getSecondValue());
            return ExchangeRate.builder()
                .buyValue(BigDecimal.ONE.divide(result.getRates().get(0).getAsk(), 10, BigDecimal.ROUND_HALF_UP))
                .sellValue(BigDecimal.ONE.divide(result.getRates().get(0).getBid(), 10, BigDecimal.ROUND_HALF_UP))
                .build();
        }
        NBPTableCCurrency result = getExchangeRates(restTemplate, exchangePair.getFirstValue());
        return ExchangeRate.builder()
            .buyValue(result.getRates().get(0).getBid())
            .sellValue(result.getRates().get(0).getAsk())
            .build();
    }

    private NBPTableCCurrency getExchangeRates(RestTemplate restTemplate, Currency currency) {
        return Optional.ofNullable(restTemplate.getForObject(String.format(NAP_API_EXCHANGE_RATES, currency), NBPTableCCurrency.class))
            .orElseThrow(() -> new CurrencyNotFoundException("There is no currency"));
    }
}
