package com.app.currencyexchange.currency.exchange.model;

import com.app.currencyexchange.currency.account.model.Action;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeValue {

    @PESEL
    private String pesel;
    @Min(1)
    private BigDecimal amount;
    private Action action;
    private ExchangePair exchangePair;

}
