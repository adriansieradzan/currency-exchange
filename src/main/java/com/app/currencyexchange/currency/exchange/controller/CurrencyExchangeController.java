package com.app.currencyexchange.currency.exchange.controller;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.exchange.model.ExchangeValue;
import com.app.currencyexchange.currency.exchange.service.CurrencyExchangeService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(CurrencyExchangeController.BASE_URL)
public class CurrencyExchangeController {

    public static final String BASE_URL = "/api/currency";

    private final CurrencyExchangeService currencyExchangeService;

    @PostMapping("/")
    public AccountDto exchangeValue(@Validated @RequestBody ExchangeValue exchangeValue) {
        return currencyExchangeService.exchangeValue(exchangeValue);
    }
}
