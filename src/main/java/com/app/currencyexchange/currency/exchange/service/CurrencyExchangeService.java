package com.app.currencyexchange.currency.exchange.service;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.exchange.model.ExchangeValue;

public interface CurrencyExchangeService {

    AccountDto exchangeValue(ExchangeValue exchangeValue);

}
