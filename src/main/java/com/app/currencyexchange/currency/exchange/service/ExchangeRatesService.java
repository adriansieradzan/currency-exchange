package com.app.currencyexchange.currency.exchange.service;

import com.app.currencyexchange.currency.exchange.model.ExchangePair;
import com.app.currencyexchange.currency.exchange.model.ExchangeRate;

public interface ExchangeRatesService {

    ExchangeRate getExchangeForCurrency(ExchangePair exchangePair);
}
