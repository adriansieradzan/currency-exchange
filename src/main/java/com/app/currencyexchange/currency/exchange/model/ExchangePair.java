package com.app.currencyexchange.currency.exchange.model;

import com.app.currencyexchange.entity.currencyaccount.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExchangePair {

    USD_PLN(Currency.USD, Currency.PLN, Boolean.FALSE),
    PLN_USD(Currency.PLN, Currency.USD, Boolean.TRUE);

    private final Currency firstValue;
    private final Currency secondValue;
    private final Boolean reversed;


}
