package com.app.currencyexchange.currency.exchange.service.impl;

import com.app.currencyexchange.currency.account.model.AccountDto;
import com.app.currencyexchange.currency.account.model.Action;
import com.app.currencyexchange.currency.account.repository.AccountRepository;
import com.app.currencyexchange.currency.account.service.mapper.AccountDtoToEntityMapper;
import com.app.currencyexchange.currency.exchange.model.ExchangeRate;
import com.app.currencyexchange.currency.exchange.model.ExchangeValue;
import com.app.currencyexchange.currency.exchange.service.CurrencyExchangeService;
import com.app.currencyexchange.currency.exchange.service.ExchangeRatesService;
import com.app.currencyexchange.entity.account.Account;
import com.app.currencyexchange.entity.currencyaccount.Currency;
import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount;
import com.app.currencyexchange.exception.BadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    @Qualifier("NBPExchangeRatesService")
    private final ExchangeRatesService exchangeRatesService;
    private final AccountRepository accountRepository;
    private final AccountDtoToEntityMapper accountDtoToEntityMapper;

    @Override
    @Transactional
    public AccountDto exchangeValue(ExchangeValue exchangeValue) {
        Account account = getAccount(exchangeValue);
        CurrencyAccount firstSubAccount = getSubAccount(account, exchangeValue.getExchangePair().getFirstValue());
        CurrencyAccount secondSubAccount = getSubAccount(account, exchangeValue.getExchangePair().getSecondValue());
        ExchangeRate courseForCurrency = exchangeRatesService.getExchangeForCurrency(exchangeValue.getExchangePair());
        if (exchangeValue.getAction().equals(Action.BUY)) {
            buyValue(exchangeValue.getAmount(), courseForCurrency, firstSubAccount, secondSubAccount);
        } else if (exchangeValue.getAction().equals(Action.SELL)) {
            sellValue(exchangeValue.getAmount(), courseForCurrency, firstSubAccount, secondSubAccount);
        } else {
            throw new IllegalArgumentException("Action can not be null");
        }
        return accountDtoToEntityMapper.accountToDto(account);
    }

    private Account getAccount(ExchangeValue exchangeValue) {
        return accountRepository.findByPesel(exchangeValue.getPesel())
            .orElseThrow(() -> new BadRequestException("Account not found"));
    }

    private CurrencyAccount getSubAccount(Account account, Currency currency) {
        return account.getCurrencyAccounts().stream()
            .filter(currencyAccount -> currencyAccount.getCurrency().equals(currency))
            .findFirst().orElseThrow(() -> new NoSuchElementException("Currency account is not exist."));
    }

    private void buyValue(BigDecimal amountToBuy, ExchangeRate exchangeRate, CurrencyAccount firstSubAccount, CurrencyAccount secondSubAccount) {
        BigDecimal amountToSell = amountToBuy.multiply(exchangeRate.getSellValue());
        validateThereIsEnoughMoneyToExchange(secondSubAccount, amountToSell);
        transferMoney(secondSubAccount, firstSubAccount, amountToSell, amountToBuy);
    }

    private void sellValue(BigDecimal amountToSell, ExchangeRate exchangeRate, CurrencyAccount firstSubAccount, CurrencyAccount secondSubAccount) {
        BigDecimal amountToBuy = amountToSell.multiply(exchangeRate.getBuyValue());
        validateThereIsEnoughMoneyToExchange(firstSubAccount, amountToSell);
        transferMoney(firstSubAccount, secondSubAccount, amountToSell, amountToBuy);
    }

    private void transferMoney(CurrencyAccount firstSubAccount, CurrencyAccount secondSubAccount, BigDecimal amountToSell, BigDecimal amountToBuy) {
        secondSubAccount.setAmount(secondSubAccount.getAmount().add(amountToBuy));
        firstSubAccount.setAmount(firstSubAccount.getAmount().subtract(amountToSell));
    }

    private void validateThereIsEnoughMoneyToExchange(CurrencyAccount subAccount, BigDecimal amountToSell) {
        if (subAccount.getAmount().compareTo(amountToSell) < 0) {
            throw new BadRequestException("Not enough money to exchange");
        }
    }

}
