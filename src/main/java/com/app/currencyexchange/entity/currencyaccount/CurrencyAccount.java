package com.app.currencyexchange.entity.currencyaccount;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;


@Entity
@Data
@Builder
@Table(name = "currency_account")
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    private Long id;
    @Column(precision = 20, scale=9)
    private BigDecimal amount;
    @Column
    @Enumerated(EnumType.STRING)
    private Currency currency;
}
