package com.app.currencyexchange.entity.account;

import com.app.currencyexchange.entity.currencyaccount.CurrencyAccount;
import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "account")
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    @NaturalId
    private String pesel;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<CurrencyAccount> currencyAccounts;

    public void addCurrencyAccount(CurrencyAccount currencyAccount) {
        if (currencyAccounts == null) {
            currencyAccounts = new HashSet<>();
        }
        currencyAccounts.add(currencyAccount);
    }
}
