package com.app.currencyexchange.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class AdultByPeselValidator implements ConstraintValidator<AdultByPesel, String> {

    private static final int PESEL_LENGTH = 11;

    @Override
    public void initialize(AdultByPesel constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        int[] pesel = convertToArrayInt(value.toCharArray());
        if (pesel.length != PESEL_LENGTH) {
            return false;
        }
        LocalDate birthDate = LocalDate.of(getBirthYear(pesel), getBirthMonth(pesel), getBirthDay(pesel));
        return birthDate.isBefore(LocalDate.now().plusDays(1).minusYears(18));
    }

    private int[] convertToArrayInt(char[] value) {
        int[] num = new int[value.length];
        for (int i = 0; i < value.length; i++) {
            num[i] = value[i] - '0';
        }
        return num;
    }

    private int getBirthYear(int[] peselNumbers) {
        int year;
        int month;
        year = 10 * peselNumbers[0];
        year += peselNumbers[1];
        month = 10 * peselNumbers[2];
        month += peselNumbers[3];
        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 0 && month < 13) {
            year += 1900;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    private int getBirthMonth(int[] peselNumbers) {
        int month;
        month = 10 * peselNumbers[2];
        month += peselNumbers[3];
        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }


    private int getBirthDay(int[] peselNumbers) {
        int day;
        day = 10 * peselNumbers[4];
        day += peselNumbers[5];
        return day;
    }
}
